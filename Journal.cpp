#include "Journal.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <chrono>
#include <iomanip>
#include <cctype>

// Default constructor
Journal::Journal()
{
	delimiter = "\t";

	listSize = 0;
}

// Default destructor
Journal::~Journal()
{
	delete [] list;
	delete [] dateList;
}

// Opens, imports, and error checks a provided file
bool Journal::fileOpen(std::string fileName)
{
	// Opens the file
	inFile.open(fileName.c_str());

	// Checks if file opens
	if (!inFile) {

		inFile.close();

		std::cout << "Error cannot open input file \"" << fileName << "\"" << std::endl;

		return false;
	}

	// Creates two buffers of the vector type to temporarily store
	// the contents for list and dateList 
	std::vector<std::tm> dateListBuffer;
	std::vector<JList> entryListBuffer;


	// Creates a line buffer string varible
	std::string lineBuffer;
	int lineNum = 1;

	// Stores the current line in a string variable buffer
	// While loop repeats until the end of the file
	while(std::getline(inFile, lineBuffer)) {
	
		// Checks if the line is blank
		if (!isStringSpace(lineBuffer)) {

			// Cycles through each character in the current line
			for (int k = 0; k < lineBuffer.size(); k++)
			{
				// If the current character is a space or a dash, skip it 
				// and then go to the next character 
				if (std::isspace(lineBuffer[k]) || (lineBuffer[k] == '-')) {
					continue;
				}

				// Checks if the first non space/dash character is a letter
				//(Means it should be the line that the date is on)
				else if (std::isalpha(lineBuffer[k])) {

				// Variable declarations

					// Keep track of the current position
					int currentPos;

					// Keep track of the position of certain breaks and delimiters
					int spacePos;
					

					// Creates and initializes string variables 
					// for the month, day, and year
					std::string monthStr = "";
					std::string dayStr = "";
					std::string yearStr = "";

					// Creates and initializes integer variables 
					// for the month, day, and year
					int monthNum = 0;
					int dayNum = 0;
					int yearNum = 0;

					// Finds position of the end of the month name by finding the next space
					spacePos = lineBuffer.find(" ", k);

					// if no space is found diplay error
					if (spacePos == std::string::npos) {

						monthStr = lineBuffer.substr(k, std::string::npos);

						std::cout << "Error expected space and rest of date after month on line: " << lineNum
							<< " somewhere in \"" << monthStr << "\"" << std::endl;

						return false;
					}

					// If a space is found store a substring in monthStr from the 
					// position of the first letter found to the first space found
						monthStr = lineBuffer.substr(k, spacePos - k);

					// Sets current position to the postition after the space after the month name
						currentPos = spacePos + 1;

					// Checks which month monthStr is
					if (monthStr == "Jan" || monthStr == "jan" || monthStr == "January" || monthStr == "january") {
						monthNum = 1;
					}
					else if (monthStr == "Feb" || monthStr == "feb" || monthStr == "February" || monthStr == "february") {
						monthNum = 2;
					}
					else if (monthStr == "Mar" || monthStr == "mar" || monthStr == "March" || monthStr == "march") {
						monthNum = 3;
					}
					else if (monthStr == "Apr" || monthStr == "apr" || monthStr == "April" || monthStr == "april") {
						monthNum = 4;
					}
					else if (monthStr == "May" || monthStr == "may") {
						monthNum = 5;
					}
					else if (monthStr == "Jun" || monthStr == "jun" || monthStr == "June" || monthStr == "june") {
						monthNum = 6;
					}
					else if (monthStr == "Jul" || monthStr == "jul" || monthStr == "July" || monthStr == "july") {
						monthNum = 7;
					}
					else if (monthStr == "Aug" || monthStr == "aug" || monthStr == "August" || monthStr == "august") {
						monthNum = 8;
					}
					else if (monthStr == "Sep" || monthStr == "sep" || monthStr == "Sept" || monthStr == "sept" || monthStr == "September" || monthStr == "september") {
						monthNum = 9;
					}
					else if (monthStr == "Oct" || monthStr == "oct" || monthStr == "October" || monthStr == "october") {
						monthNum = 10;
					}
					else if (monthStr == "Nov" || monthStr == "nov" || monthStr == "November" || monthStr == "november") {
						monthNum = 11;
					}
					else if (monthStr == "Dec" || monthStr == "dec" || monthStr == "December" || monthStr == "december") {
						monthNum = 12;
					}
					// If monthStr is not a valid month name display error
					else {

						std::cout << "Error \"" << monthStr << "\" on line: " << lineNum << " is not a valid month name" << std::endl;
						return false;
					}

					// Find next position that is not a space
					if (lineBuffer[currentPos] == ' ') {
						currentPos = lineBuffer.find_first_not_of(" ", currentPos);

						// If the rest of the line is just spaces display error
						if (currentPos == std::string::npos) {

							std::cout << "Error day number after month " << monthStr << " is not present on line: "
								<< lineNum << std::endl;

							return false;
						}

					}

					// Checks if the next nonspace character is a number
					if (!std::isdigit(lineBuffer[currentPos])) {
						std::cout << "Error expected day number after month \""
							<< monthStr << "\" on line: " << lineNum << std::endl;
						return false;
					}

					// Adds the chracter in the current position to dayStr
					dayStr += lineBuffer[currentPos];

					// Increments the current position
					currentPos++;

					// Checks if the next postion is a space (Meaning there is only a single digit representing day)
					if (lineBuffer[currentPos] == ' ') {
						// Do nothing
					}
					// if there is not a digit or space after the first day digit display error
					else if (!std::isdigit(lineBuffer[currentPos])) {
						std::cout << "Error expected day number after month \""
							<< monthStr << "\" on line: " << lineNum << std::endl;
						return false;
					}
					// if there is a second digit representing day add it to dayStr
					else {
						dayStr += lineBuffer[currentPos];

						// Go to next position
						currentPos++;
					}


					// Convert day string to integer and store it in dayNum
					dayNum = std::stoi(dayStr);


					// Initial check for day validity
					if (dayNum < 1 || dayNum > 31) {
						std::cout << "Error the day number: "
							<< dayNum << ", on line: " << lineNum << " is out of the range of 1 to 31" << std::endl;
						return false;
					}

					// Checks if there is a space and or a comma separating date number and year 
					if (lineBuffer[currentPos] == ',') {
						currentPos++;
					}

					if (lineBuffer[currentPos] != ' ') {

						std::cout << "Error expected space after day number: "
							<< dayNum << ", on line: " << std::endl;
						return false;
					}

					// Go to next position
					currentPos++;

					// Find next position that is not a space
					if (lineBuffer[currentPos] == ' ') {
						currentPos = lineBuffer.find_first_not_of(" ", currentPos);
					}

					// Checks if the first non space character is a digit
					if (!std::isdigit(lineBuffer[currentPos])) {
						std::cout << "Error expected year after day number: "
							<< monthStr << " on line: " << lineNum << std::endl;
						return false;
					}

					// Adds first string digit to yearStr 
					yearStr += lineBuffer[currentPos];

					// Go to next position
					currentPos++;

					// Checks if the next character is a digit
					if (!std::isdigit(lineBuffer[currentPos])) {
						std::cout << "Error year must be at least two digits instead of one: \""
							<< monthStr << "\" on line: " << lineNum << std::endl;

						return false;
					}

					// Adds second string digit to yearStr 
					yearStr += lineBuffer[currentPos];

					// Go to next position
					currentPos++;

					// check if there is an additional two digit after the first two
					if (std::isdigit(lineBuffer[currentPos])) {
						if (std::isdigit(lineBuffer[currentPos + 1])) {

							// Adds third string digit to yearStr 
							yearStr += lineBuffer[currentPos];

							// Go to next position
							currentPos++;

							// Adds fourth string digit to yearStr 
							yearStr += lineBuffer[currentPos];
						}
					}


					// Converts yearStr to an integer stored in yearNum 
					yearNum = std::stoi(yearStr);

					// Converts two digit year to four digit year
					if (yearNum < 100) yearNum += 2000;

					// Checks if year is before 1900
					if (yearNum < 1900) {
						std::cout << "Error year cannot be before 1900 on line: " << lineNum << std::endl;
						return false;
					}

					// Checks if the accidentally put febuary 29th on a non leap year and displays appropriate error
					if ((monthNum == 2) && (dayNum == 29) && !((yearNum % 4) == 0 && !((yearNum % 100) == 0 && (yearNum % 400) != 0))) {
						std::cout << "Error febuary only has 29 days on a leap year on line: " << lineNum << std::endl;
						return false;
					}

					// Checks if the date is valid
					if (!checkDate(monthNum, dayNum, yearNum)) {
						std::cout << "Error the date " << monthStr << ", " << dayNum << " " << yearNum 
							<< " on line " << lineNum << " is not a valid date" << std::endl;
						return false;
					}



					// Creates a temporary tm structure given the month day and year
					std::tm tempDate = dateToTm(monthNum, dayNum, yearNum);

					// Checks if the previous date is greater than the date just read
					if (dateListBuffer.size() > 0) {
						
						if (isDateGreater(dateListBuffer.back(), tempDate)) {
							std::cout << "Error the date " << monthStr << ", " << dayNum << " " << yearNum 
								<< " on line " << lineNum << " is not in order with previous dates " << std::endl;
							return false;
						}
						
					}

					// Checks if the date just read in in greater the current date (its in the future)
					if (isDateGreater(tempDate, getCurrentTime())) {
							std::cout << "Error the date " << monthStr << ", " << dayNum << " " << yearNum
								<< " on line " << lineNum << " is in the future " << std::endl;
							return false;
					}

					if (dateListBuffer.size() > 0) {
						// If the date is the same as the last one do not add it
						if (areDatesEqual(dateListBuffer.back(), tempDate)) {
							break;
						}
					}

					// Creates a temporary list object
					JList tempJList;

					// appends the tm structure tempDate to the back if the dateListBuffer
					dateListBuffer.push_back(tempDate);

					// appends the list object tempJList to the back if the entryListBuffer
					entryListBuffer.push_back(tempJList);

					// Exits the for loop
					break;

				}
			
				// Checks if the first non space/dash character is a number
				//(Means it should be the line that the time is on and the begining of the entry)
				else if (std::isdigit(lineBuffer[k])) {

					// Creates current position and stores
					// the position of the first number character on the line
					int currPos = k;

					// Creates space position integer variable
					int sPos;

					// Creates and initializes string variables 
					// for the entry, the hour, and the minute
					std::string entryStr = "";
					std::string hourStr = "";
					std::string minStr = "";
			
					// Creates and initializes integer variables 
					// for the hour, and the minute
					int hourNum = 0;
					int minNum = 0;

					// Adds the first number character on the line to the hourStr variable
					hourStr += lineBuffer[currPos];

					// Increments current position by one
					currPos++;

					// Checks if there is a colon seperating the minutes and the hours
					// or if there is at least 3 numbers 

					// Checks if there is a colon after the first digit
					if (lineBuffer[currPos] == ':') {

						// Checks if there are two digits after the colon 
						if ((std::isdigit(lineBuffer[currPos + 1])) && (std::isdigit(lineBuffer[currPos + 2]))) {

							currPos++;
							minStr += lineBuffer[currPos];
							currPos++;
							minStr += lineBuffer[currPos];
						}
						else {
							std::cout << "Error there must be exactly 2 digits after the colon in the time on the line: " << lineNum << std::endl;
							return false;
						}
					}

					// Checks if there is a colon after the first two digits
					else if ((lineBuffer[currPos + 1] == ':') && (std::isdigit(lineBuffer[currPos]))) {
					
						hourStr += lineBuffer[currPos];
						currPos++;

						// Checks if there are two digits after the colon 
						if ((std::isdigit(lineBuffer[currPos + 1])) && (std::isdigit(lineBuffer[currPos + 2]))) {

							currPos++;
							minStr += lineBuffer[currPos];
							currPos++;
							minStr += lineBuffer[currPos];
						}
						else {
							std::cout << "Error there must be exactly 2 digits after the colon in the time on the line: " << lineNum << std::endl;
							return false;
						}

					}

					// Checks if there are 4 digits total without a colon
					else if ( (std::isdigit(lineBuffer[currPos])) && (std::isdigit(lineBuffer[currPos + 1])) && (std::isdigit(lineBuffer[currPos + 2]))) {
				
						hourStr += lineBuffer[currPos];
						currPos++;
						minStr += lineBuffer[currPos];
						currPos++;
						minStr += lineBuffer[currPos];
					}

					// Checks if there are 3 digits total without a colon
					else if ((std::isdigit(lineBuffer[currPos])) && (std::isdigit(lineBuffer[currPos + 1]))) {
	
							minStr += lineBuffer[currPos];
							currPos++;
							minStr += lineBuffer[currPos];

					}
					else {
						std::cout << "Error time must have at least 3 digits on line: " << lineNum << std::endl;
						return false;
					}


					// Converts the string variables, hourStr and minStr 
					// to the integer variables hourNum and minNum
					hourNum = std::stoi(hourStr);
					minNum = std::stoi(minStr);

					// Increments current position by one
					currPos++;

					// Finds the first position of a non space chracter and stores it in sPos
					sPos = lineBuffer.find_first_not_of(" ", currPos);

					// If there the rest of the line is just spaces do nothing for now
					if (sPos == std::string::npos) {
						// do Nothing
					}

					// Checks if there is an AM after the time 
					else if ((lineBuffer[sPos] == 'a' || lineBuffer[sPos] == 'A') && (lineBuffer[sPos + 1] == 'm' || lineBuffer[sPos + 1] == 'M')) {
					
						// If the time is 12 am set it to 0 
						if (hourNum == 12) {
							hourNum = 0;
						}

						// Increments current position by two
						currPos = sPos + 2;


					}
					// Checks if there is an PM after the time 
					else if ((lineBuffer[sPos] == 'p' || lineBuffer[sPos] == 'P') && (lineBuffer[sPos + 1] == 'm' || lineBuffer[sPos + 1] == 'M')) {
					
						// If the hour is not 12 add twelve to the hour
						if (hourNum != 12) {
							hourNum += 12;
						}
					
						// Increments current position by two
						currPos = sPos + 2;
					}

					// If the hour is 24 set it to zero
					if (hourNum == 24) {
						hourNum = 0;
					}



					// Checks if the hour is valid
					if (hourNum < 0 || hourNum > 23) {
						std::cout << "Error hour in the time is not valid on line " << lineNum << std::endl;
					}

					// Checks if the minute is valid
					if (hourNum < 0 || hourNum > 23) {
						std::cout << "Error minute in the time is not valid on line " << lineNum << std::endl;
					}

					// Finds the dash separating the time and the entry and stores the position in sPos
					sPos = lineBuffer.find("-", currPos);


// mark

					// If there is no dash separating the time and the entry then 
					// just find the first non character space character and store the rest of the line in entry string
					if(sPos == std::string::npos){
						sPos = lineBuffer.find_first_not_of(" ", currPos);
						useDelimiter ? entryStr = extractEntry(lineNum, lineBuffer, currPos) : entryStr = lineBuffer.substr(currPos, std::string::npos);
					}
					// If the dash is found
					else
					{
						// Set the current position to the position of the dash
						currPos = sPos;

						// Find the first position after the dash that is not a space
						sPos = lineBuffer.find_first_not_of(" ", sPos + 1);

						// If the rest of the line after the dash are just spaces
						if (sPos == std::string::npos) {
							currPos++;
							useDelimiter ? entryStr = extractEntry(lineNum, lineBuffer, currPos) : entryStr = lineBuffer.substr(currPos, std::string::npos);
						}
						else
						{
							// Set the current position to the first position after the dash that is not a space
							currPos = sPos;

							
							useDelimiter ? entryStr = extractEntry(lineNum, lineBuffer, currPos) : entryStr = lineBuffer.substr(currPos, std::string::npos);
						}
					}

					// Make a new tm structure from the time just extracted and the last date stored 
					std::tm timeTemp =  addTimeToDate(dateListBuffer.back(), hourNum, minNum);

					// Moves to most recent date added to list
					entryListBuffer.back().goToBack();

					
					if (entryListBuffer.back().getCount() > 1) {
					// Checks if the previous time is greater than the time just read in
					if (isTimeGreater(entryListBuffer.back().getTime(), timeTemp)) {
						std::cout << "Error the time " << hourStr << ":" << minStr << " on line: " << lineNum << " is out of order with other times" << std::endl;
						return false;
					}
					}

					// Append the date and the entry string to the list associted with the last date stored
					entryListBuffer.back().append(entryStr, timeTemp);

						
					// Exits the for loop
					break;
				}
			}

		}
		// Checks if the first line is empty
		else if (lineNum == 1)
		{
			std::cout << "Error the first line must contain a date" << std::endl;
			return false;
		}


		// Increments line number
		lineNum++;
	}

	// Creates two new arrays with the size of thier repective buffers
	dateList = new std::tm[dateListBuffer.size()];
	list = new JList[entryListBuffer.size()];

	// Transfers values from the two buffers to the two arrays
	for (int b = 0; b < dateListBuffer.size(); b++)
	{
		dateList[b] = dateListBuffer[b];
		list[b] = entryListBuffer[b];
	}
	
	// Sets the list size to the size of the buffer
	listSize = dateListBuffer.size();

	// Closes the file
	inFile.close();

	// Saves fileName
	JfileName = fileName;

	// returns true signaling success
	return true;
}

void Journal::newJ(std::string fileName)
{
	// Clears out the current journal
	clear();

	// Opens new file 
	outFile.open(fileName.c_str());


	// (list and dateList are parallel arrays)
	// Create a new array of JList object of size 1
	list = new JList[1];

	// Create a new array of JList object of size 1
	dateList = new tm[1];

	listSize = 1;

	std::tm tempTime;

	tempTime = getCurrentTime();

	// Save tempTime to first position in dateList
	dateList[0] = tempTime;

	// Outputs formatted time to journal text file using tempTime
	outFile << std::put_time(&tempTime, "--------- %B %d, %Y ---------") << '\n';

	// Closes the file
	outFile.close();

	// Saves fileName
	JfileName = fileName;
}

void Journal::clear()
{
}

// Adds entry to current date
bool Journal::addEntry(std::string ent)
{


	// Checks if any dates exists
	if (listSize > 0) {

	
		removeDelimiter(ent);

		// Checks if the latest date in the dateList is equal to current date
		if (areDatesEqual(dateList[listSize - 1], getCurrentTime())) {
			
	

			// Appends the entry at the end of the entry list for the latest date
			list[listSize - 1].append(ent, getCurrentTime());

			//[test]
			list[listSize - 1].goToFront();
			

			//[test]
			std::cout << list[listSize - 1].getE() << std::endl;

			return true;
		}
		else {

			// Checks if the latest date in the dateList is greater than the current date(more recent)
			// (meaning the system clock is supposedly in the past or the latest date is wrong)
			if (isDateGreater(dateList[listSize - 1], getCurrentTime()))
				return false;
			else
			{
				// Adds current date to dateList and list
				addCurrentDate();

				// Appends the entry at the end of the entry list for the latest date
				list[listSize - 1].append(ent, getCurrentTime());

				return true;

			}
		
		}
	}
	else {
		return false;
	}
}

bool Journal::save()
{


	if (listSize > 0)
	{
		outFile.open(JfileName.c_str());

		if (!outFile) {
			outFile.close();
			return false;
		}
		 
		// Outputs and formats journal contents to file
		for (int i = 0; i < listSize; i++)
		{
			if (list[i].getCount() > 0) {

				// Prints date in proper format
				outFile << std::put_time(&dateList[i], "--------- %B %d, %Y ---------") << "\n\n";
				
				list[i].goToFront();
				
				
				for (int j = 0; j < list[i].getCount(); j++)
				{
					std::tm temp = list[i].getTime();

					// Prints time in proper format
					outFile << std::put_time(&temp, " %I:%M %p - ");
					
					// Prints entry in proper format
					outFile << list[i].getE() << delimiter << "\n";

					if (list[i].getNextPointer() != NULL) {
						list[i].next();
					}
				}
			}
		}
		outFile.close();

		return true;
	}
	else
	return false;
}

void Journal::printAll()
{
	// [test]
	int eCount = 0;

	if (listSize < 1) {
		return;
	}

	// Outputs and formats journal contents to file
	for (int i = 0; i < listSize; i++)
	{
		if (list[i].getCount() > 0) {

			std::cout << std::put_time(&dateList[i], "--------- %B %d, %Y ---------") << std::endl << std::endl;

			list[i].goToFront();


			for (int j = 0; j < list[i].getCount(); j++)
			{
				std::tm temp = list[i].getTime();


				std::cout << std::put_time(&temp, " %I:%M %p - ");


				std::cout << list[i].getE() << std::endl  << std::endl;

				eCount++;

				if (list[i].getNextPointer() != NULL) {
					list[i].next();
				}

			}

		}

	}

	// [test]
	std::cout << "dates: (" << listSize << ")       entries: (" << eCount << ")" << std::endl;
}

std::string Journal::getDelimiter() const
{
	return delimiter;
}

void Journal::removeDelimiter(std::string& entry, std::string replacement)
{
	bool found = true;
	int delimPos = 0;

	while (found) {
		
		delimPos = entry.find(delimiter, delimPos);

			if (delimPos == std::string::npos) {
				
				
				
				found == false;
				return;
			}


			entry.erase(delimPos, delimiter.size());
			entry.insert(delimPos, replacement);
	}

}

bool Journal::printDate(int month, int day, int year)
{
	// Checks if the journal is empty
	if (listSize < 1) return false;
	
	// Creates temporary variable for date entered
	std::tm dateTemp = dateToTm(month, day, year);

	// compares each date in dateList to find a match
	// [(Note to self): should I include a binary search here]
	for (int i = 0; i < listSize; i++)
	{
		// checks for match
		if (areDatesEqual(dateTemp, dateList[i])) {
			
			// Prints date header
			std::cout << std::put_time(&dateList[i], "--------- %B %d, %Y ---------") << std::endl << std::endl;

			// Moves to front of linked list 
			list[i].goToFront();

			// Prints out all entries asosiated with a certian date
			for (int j = 0; j < list[i].getCount(); j++)
			{
				// Gets the time associated with the entry and stores it in a temporary variable
				std::tm temp = list[i].getTime();

				// Prints time header
				std::cout << std::put_time(&temp, " %I:%M %p - ");

				// Prints entry string 
				std::cout << list[i].getE() << std::endl << std::endl;

				// Moves to next entry if it is not null
				if (list[i].getNextPointer() != NULL) {
					list[i].next();
				}

			}

			return true;
		
		}
	}
	
	return false;
}

bool Journal::printDate(std::tm date)
{
	// Checks if the journal is empty
	if (listSize < 1) return false;
	

	// compares each date in dateList to find a match
	// [(Note to self): should I include a binary search here]
	for (int i = 0; i < listSize; i++)
	{
		// checks for match
		if (areDatesEqual(date, dateList[i])) {

			// Prints date header
			std::cout << std::put_time(&dateList[i], "--------- %B %d, %Y ---------") << std::endl << std::endl;

			// Moves to front of linked list 
			list[i].goToFront();

			// Prints out all entries asosiated with a certian date
			for (int j = 0; j < list[i].getCount(); j++)
			{
				// Gets the time associated with the entry and stores it in a temporary variable
				std::tm temp = list[i].getTime();

				// Prints time header
				std::cout << std::put_time(&temp, " %I:%M %p - ");

				// Prints entry string 
				std::cout << list[i].getE() << std::endl << std::endl;

				// Moves to next entry if it is not null
				if (list[i].getNextPointer() != NULL) {
					list[i].next();
				}

			}

			return true;

		}
	}

	return false;
}

bool Journal::printDate(int index)
{
	// Checks if the journal is empty
	if (index >= listSize || index < 0) return false;

	// Prints date header
	std::cout << std::put_time(&dateList[index], "--------- %B %d, %Y ---------") << std::endl << std::endl;

	// Moves to front of linked list 
	list[index].goToFront();

	// Prints out all entries asosiated with a certian date
	for (int j = 0; j < list[index].getCount(); j++)
	{
		// Gets the time associated with the entry and stores it in a temporary variable
		std::tm temp = list[index].getTime();

		// Prints time header
		std::cout << std::put_time(&temp, " %I:%M %p - ");

		// Prints entry string 
		std::cout << list[index].getE() << std::endl << std::endl;

		// Moves to next entry if it is not null
		if (list[index].getNextPointer() != NULL) {
			list[index].next();
		}
	}
		return true;
}

bool Journal::printDateRange(int month1, int day1, int year1, int month2, int day2, int year2)
{
	// Checks if the journal is empty
	if (listSize < 1) return false;

	// Creates temporary variables for dates entered
	std::tm date1 = dateToTm(month1, day1, year1);

	std::tm date2 = dateToTm(month2, day2, year2);

	bool isNotEmpty = false;

	// compares each date in dateList to find a match
	for (int i = 0; i < listSize; i++)
	{
		// Checks if date is in range
		if ((isDateGreater(dateList[i], date1) || areDatesEqual(dateList[i], date1)) && ((isDateGreater(date2, dateList[i]) || areDatesEqual(date2, dateList[i]))))
		{
			printDate(i);
			isNotEmpty = true;
		}
	}
	return isNotEmpty;
}

bool Journal::printDateRange(std::tm date1, std::tm date2)
{
	// Checks if the journal is empty
	if (listSize < 1) return false;

	bool isNotEmpty = false;

	// compares each date in dateList to find a match
	for (int i = 0; i < listSize; i++)
	{
		// Checks if date is in range
		if ((isDateGreater(dateList[i], date1) || areDatesEqual(dateList[i], date1)) && ((isDateGreater(date2, dateList[i]) || areDatesEqual(date2, dateList[i]))))
		{
			printDate(i);
			isNotEmpty = true;
		}
	}
	return isNotEmpty;
}

int Journal::getDateCount() const
{
	return listSize;
}

int Journal::getEntryCount()
{
	int eCount = 0;
	for (int i = 0; i < listSize; i++)
	{
		eCount += list[i].getCount();

	}
	return eCount;
}

// Checks if sting is only white space
bool Journal::isStringSpace(std::string str)
{

	for ( int i = 0;  i < str.size(); i++)
	{
		if (!std::isspace(str[i])) {
			return false;
		}
	}

	return true;
}

// Adds current date to dateList and list
void Journal::addCurrentDate()
{
	// Creates two temporary variables 
	JList*  tempList = new JList[listSize + 1];
	std::tm* tempDateList = new std::tm[listSize + 1];

	
	// Copies data from list and dateList to temporary variables
	for (int i = 0; i < listSize; i++)
	{
		tempList[i] = list[i];
		tempDateList[i] = dateList[i];
	}

	// Puts current date at end tempDateList
	tempDateList[listSize] = getCurrentTime();

	// Deallocates the old memory in list and dateList
	delete[] list;
	delete[] dateList;

	// Sets list and dateList to pointers in temporary varibles
	list = tempList;
	dateList = tempDateList;

	// Increments listSize
	listSize++;
}

// Gets local current time and returns it as a tm structure
std::tm Journal::getCurrentTime()
{
	// Get current time as an "std::chrono::time_point" object named "now"
	auto now = std::chrono::system_clock::now();

	// Converts the "now" object to a time_t object (Unix timestamp) named "now_t"
	std::time_t now_t = std::chrono::system_clock::to_time_t(now);

	// Converts the "now_t" object to a local time stored 
	// in a tm structure named "local"
	std::tm local;
	localtime_s(&local, &now_t);

	return local;
}


// Checks if the dates of two tm structures are equal
bool Journal::areDatesEqual(std::tm d1, std::tm d2)
{
	// Checks if d1's years since 1900 is equal to d2's
	if (d1.tm_year == d2.tm_year) {

		// Checks if d1's number of days since january 1 is equal to d2's
		if (d1.tm_yday == d2.tm_yday)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool Journal::areTimesEqual(std::tm d1, std::tm d2)
{
	// Checks if d1 hour number is equal to d2's
	if (d1.tm_hour == d2.tm_hour) {

		// Checks if d1's minutes is equal to d2's
		if (d1.tm_min == d2.tm_min)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

// Checks if the date of one tm structure is greater than another (more recent)
bool Journal::isDateGreater(std::tm d1, std::tm d2)
{
	// Checks if d1's years since 1900 is greater than d2's
	if (d1.tm_year > d2.tm_year) {

		return true;
	}
	else
	{	
			// Checks if d1's number of days since january 1 is greater than d2's
		if (d1.tm_yday > d2.tm_yday && d1.tm_year == d2.tm_year)
		{
			
			return true;
		}
		else
		{
			return false;
		}
	}
}

bool Journal::isTimeGreater(std::tm d1, std::tm d2)
{
	// Checks if d1's hour is greater than d2's
	if (d1.tm_hour > d2.tm_hour) {

		return true;
	}
	else
	{
		// Checks if d1's minutes are greater than d2's
		if (d1.tm_min > d2.tm_min && d1.tm_hour == d2.tm_hour)
		{

			return true;
		}
		else
		{
			return false;
		}
	}
}

bool Journal::checkDate(int month, int day, int year)
{
	// Checks if year is less than 1900
	if (year < 1900) 
		return false;

	if (month > 12  || month < 0) 
		return false;
	
	if (day > 31 || day < 0)
		return false;


	if (month == 1) {
		if (day <= 31) 
			return true;
		else
			return false;
	}
	else if (month == 2) {
		// Checks if year is leap year
		if ((year % 4) == 0 && !((year % 100) == 0 && (year % 400) != 0)) {
			
			
			if (day <= 29)
				return true;
			else
				return false;
		}
		else {
			if (day <= 28)
				return true;
			else
				return false;
		}
	}
	else if (month == 3) {
		if (day <= 31)
			return true;
		else
			return false;
	}
	else if (month == 4) {
		if (day <= 30)
			return true;
		else
			return false;
	}
	else if (month == 5) {
		if (day <= 31)
			return true;
		else
			return false;
	}
	else if (month == 6) {
		if (day <= 30)
			return true;
		else
			return false;
	}
	else if (month == 7) {
		if (day <= 31)
			return true;
		else
			return false;
	}
	else if (month == 8) {
		if (day <= 31)
			return true;
		else
			return false;
	}
	else if (month == 9) {
		if (day <= 30)
			return true;
		else
			return false;
	}
	else if (month == 10) {
		if (day <= 31)
			return true;
		else
			return false;
	}
	else if (month == 11) {
		if (day <= 30)
			return true;
		else
			return false;
	}
	else if (month == 12) {
		if (day <= 31)
			return true;
		else
			return false;
	}
	
}

std::tm Journal::dateToTm(int month, int day, int year)
{
	std::tm dt = {};

	dt.tm_mon = month - 1;

	dt.tm_mday = day;

	dt.tm_year = year - 1900;

	dt.tm_yday = getTotalDays(month, day, year) - 1;

	dt.tm_hour = 0;

	dt.tm_min = 0;

	dt.tm_sec = 0;

	return dt;
}

int Journal::getTotalDays(int month, int day, int year)
{
	bool isLeap = false;

	// Checks if year is leap year
	if ((year % 4) == 0 && !((year % 100) == 0 && (year % 400) != 0)) {
		isLeap = true;
	}

	if (month == 1) {
		return day;
	}
	else if (month == 2) {
		return day + 31;
	}
	else if (month == 3) {
	
		if (isLeap) return day + 60;
		else return day + 59;
	}
	else if (month == 4) {
		
		if (isLeap) return day + 91;
		else return day + 90;
	}
	else if (month == 5) {
	
		if (isLeap) return day + 121;
		else return day + 120;
	}
	else if (month == 6) {
		if (isLeap) return day + 152;
		else return day + 151;
	}
	else if (month == 7) {
		
		if (isLeap) return day + 182;
		else return day + 181;
	}
	else if (month == 8) {
	
		if (isLeap) return day + 213;
		else return day + 212;
	}
	else if (month == 9) {
	
		if (isLeap) return day + 244;
		else return day + 243;
	}
	else if (month == 10) {
	
		if (isLeap) return day + 274;
		else return day + 273;
	}
	else if (month == 11) {
		
		if (isLeap) return day + 305;
		else return day + 304;
	}
	else if (month == 12) {
	
		if (isLeap) return day + 335;
		else return day + 334;
	}
	else return 1;
}

std::tm Journal::addTimeToDate(std::tm date, int hour, int min)
{
	std::tm tempTime = date;

	tempTime.tm_hour = hour;

	tempTime.tm_min = min;

	return tempTime;
}

std::string Journal::extractEntry(int& lineNumber, std::string firstLine, int position = 0)
{
	
	std::string entry = "";
	std::string lineStr;
	int dePos;
	
	dePos = firstLine.find(delimiter, position);

	if (dePos == std::string::npos) {

		
		entry += firstLine.substr(position, std::string::npos);
		entry += "\n";
	}
	else
	{

		entry += firstLine.substr(position, dePos);
		
		return entry;
	}
	
	while (std::getline(inFile, lineStr)) {
		
	

		lineNumber++;



		dePos = lineStr.find(delimiter);




		if (dePos == std::string::npos) {

		


			entry += lineStr.substr(0, std::string::npos);

			entry += "\n";

	
		}
		else
		{
			entry += lineStr.substr(0, dePos);

			return entry;
		}
	}

	return entry;
}




