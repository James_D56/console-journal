#pragma once
#include "JList.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <iomanip>
#include <chrono>
#include <cctype>

class Journal
{
public:
	// Default constructor and destructor
	Journal();
	~Journal();

	bool fileOpen(std::string fileName);

	void newJ(std::string fileName);

	void clear();

	bool addEntry(std::string ent);

	bool save();

	void printAll();

	std::string getDelimiter() const;

	void removeDelimiter(std::string& entry, std::string replacement = " ");

	bool printDate(int month, int day, int year);

	bool printDate(std::tm date);

	bool printDate(int index);
	
	bool printDateRange(int month1, int day1, int year1, int month2, int day2, int year2);

	bool printDateRange(std::tm date1, std::tm date2);

	int getDateCount() const;

	int getEntryCount();

	void getCurrentDate();


private:
	JList* list;
	std::tm* dateList;

	int listSize;

	bool useDelimiter = true;

	

	std::ifstream inFile;
	std::ofstream outFile;

	std::string JfileName;

	std::string delimiter;

	// helper functions

	bool isStringSpace(std::string str);

	void addCurrentDate();

	// Gets local current time and returns it as a tm structure
	std::tm getCurrentTime();

	bool areDatesEqual(std::tm d1, std::tm d2);
	bool areTimesEqual(std::tm d1, std::tm d2);

	bool isDateGreater(std::tm d1, std::tm d2);

	bool isTimeGreater(std::tm d1, std::tm d2);

	bool checkDate(int month, int day, int year);

	std::tm dateToTm(int month, int day, int year);

	int getTotalDays(int month, int day, int year);

	std::tm addTimeToDate(std::tm date, int hour, int min);

	std::string extractEntry(int& lineNumber, std::string firstLine, int position);


};

