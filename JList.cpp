#include "JList.h"
#include <chrono>


JList::JList()
{
	count = 0;
	head = new Node;
	current = NULL;
	tail = NULL;
}

JList::~JList()
{
}

void JList::append(std::string entry, std::tm ti)
{
	Node* temp = new Node(entry, NULL, ti);
	if (count == 0) {
		head->setLink(temp);
		current = temp;
		tail = temp;
		
	}
	else {
		tail->setLink(temp);
		tail = temp;
	}
	count++;
}

void JList::clear()
{
	if (count != 0){

	goToFront();


	clearHelp(current);
	current = NULL;
	tail = NULL;

	count = 0;
	}
}

void JList::goToFront()
{

	current = head->getLink();
}

void JList::goToBack()
{
	current = tail;
}

void JList::next()
{
	if(current != tail)
	current = current->getLink();
}

int JList::getCount() const
{
	return count;
}

std::string JList::getE() const
{
	return current->getEntry();

}

std::tm JList::getTime() const
{
	if(current != nullptr){
		return current->getTim();
	}
	std::tm emptyTm = {};
	return emptyTm;
}

Node* JList::getNextPointer() const
{
	return current->getLink();
}

const JList& JList::operator=( JList& otherList)
{


	if (this != &otherList) {

		
		clear();
	
		otherList.goToFront();
			for (int i = 0; i < otherList.count; i++)
			{
					append(otherList.getE(), otherList.getTime());

					if (otherList.getNextPointer() != NULL) {
						otherList.next();
					}
			}
	}
	return *this;
}

void JList::clearHelp(Node* curr)
{
	std::cout << "enterend clearHelp" << std::endl;
	if (curr->getLink() != NULL) {
		std::cout << "passed null in clearHelp" << std::endl;
		clearHelp(curr->getLink());
	}
	std::cout << "deleting clearHelp" << std::endl;
	delete curr;
}
