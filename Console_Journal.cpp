//  Console_Journal.cpp
// 
// Author: James Davidson
// Date Created: 5/21/2024 10:36
// 
// Last Modified: 5/21/2024


#include <iostream>
#include <fstream>
#include "Node.h"
#include "JList.h"
#include "Journal.h"
#include <chrono>

#include <iomanip>

using namespace std;

// Functions

// Gets a yes or no responce
bool YorN(string message);

// Trims a string
void trim(string& str);

// checks if string has the .txt extention 
bool isTxt(string str);

// Checks if sting is only white space
bool isStrSpace(std::string str);

int main()
{
	// Variable Declerations
	ifstream inFile;
	ofstream outFile;
	string journalFileName;
	string configFileName;

	Journal jn;

    // Opens the file
    inFile.open("journal_config.txt");

	// Checks if configuration files exists and if 
    if (!inFile) {
        
		// Closes file
		inFile.close();

		// Prints message
		cout << " There does not seem to be a configuration file"
            << " present in the current directory" << endl;

		// Asks user if they would like to create a new confinguration file and journal text file
		if (YorN(" Would you like to create a new confinguration file and journal text file (y/n): ")) {
			
			// Adds some space
			cout << endl;

			// Checks if user wants to use an exiting journal file instead
			if (!YorN(" Would you like to use an existing journal text file instead of creating a new one (y/n): "))
			{
				// Adds some space
				cout << endl;

				do {
				cout << " Please enter the name and location of the new text file: ";
				cin >> journalFileName;

				} while (journalFileName.empty() || isStrSpace(journalFileName));

				trim(journalFileName);

				if (!isTxt(journalFileName)) {
					journalFileName = journalFileName + ".txt";
				}
					
				outFile.open("journal_config.txt");

				if (!outFile) {
					cout << " Error could not create configuration file" << endl;
					return 1;
				}

				outFile << journalFileName;

				outFile.close();

				cout << " Creating file \"" << journalFileName << "\"" << endl;
				
				jn.newJ(journalFileName);
					
				jn.addEntry("hello");

				jn.addEntry("there once was a shippp");

				 jn.save();

			}
			else
			{
				
			}





		}
		else {
			// Exits program if user does not want to create a new confinguration file and journal text file
			cout << "exiting program..." << endl;
			
			return 0;
		}

     }
	
	else {

	 

		getline(inFile, journalFileName);

		trim(journalFileName);

		cout << " File name for opening \"" << journalFileName << "\"" << endl << endl;

		if (!jn.fileOpen(journalFileName)) {
			cout << endl << " Please fix the errors above in the file \"" << journalFileName << "\"" << endl;
		}

		string entryS;

		

		//system("CLS");
		

		cout << "entry count: " << jn.getEntryCount() << endl;

		if (!jn.printDateRange(5, 1, 2024, 5, 29, 2024)) cout << endl << endl << "no entries matching that date" << endl << endl;

	
		//jn.printAll();

		/*

		
		cout << endl << endl << " Enter entry: ";

		

		getline(cin, entryS);

		jn.addEntry(entryS);
		//system("CLS");

		jn.printAll();



		jn.save();

		cout << "entry count: " << jn.getEntryCount() << endl;*/
		
	}
	return 0;
}






// Gets a yes or no responce
bool YorN(string message)
{
	bool isValid;
	char answer;
	bool choice;
	do
	{
		// Sets isValid to true
		isValid = true;

		// Prints instructions
		cout << message;

		// Gets input from user
		cin >> answer;

		// Checks for valid answer
		switch (answer)
		{
		case 'y':
		case 'Y':
			choice = true;

			// Clears input streams just in case they aswered yes instead of y
			cin.clear();

			// Ignores the rest of the input until the newline \n character is read
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			break;
		case 'n':
		case 'N':
			choice = false;

			// Clears input streams just in case they aswered yes instead of n
			cin.clear();

			// Ignores the rest of the input until the newline \n character is read
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			break;

		default:
			// Clears input streams
			cin.clear();

			// Ignores the rest of the input until the newline \n character is read
			cin.ignore(numeric_limits<streamsize>::max(), '\n');

			// Sets isValid to false
			isValid = false;

			// Prints error message
			cout << endl << "Please only input y or n" << endl << endl;

		}

	} while (!isValid);
	return choice;

}


// Trims a string

void trim(string& str) {

	int front = 0;
	int back = 0;
	int current;

	if (!str.empty()) {


		// Sets current position to front
		current = 0;

		while (isspace(str[current]) && (current < str.size())) {
			front++;
			current++;
		}

		str.erase(0, front);

		if (!str.empty()) {
			// Sets current position to back
			current = static_cast<int>(str.size() - 1);

			while (isspace(str[current]) && (current < str.size())) {
				back++;
				current--;
			}

			str.erase(current + 1, back);
		}
	}
}

// checks if string has the .txt extention 
bool isTxt(string str)
{
	string ext;
	if (str.size() > 4) {
		ext = str.substr((str.size() - 4), 4);

		if (ext == ".txt") return true;
		else return false;
			
	}
	else return false;
}

// Checks if sting is only white space
bool isStrSpace(std::string str)
{

	for (int i = 0; i < str.size(); i++)
	{
		if (!std::isspace(str[i])) {
			return false;
		}
	}

	return true;
}
