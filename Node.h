#pragma once

#include <iostream>
#include <string>
#include <chrono>

class Node
{
public:
	Node();
	Node(std::string str, Node* lnk);
	Node(std::string str, Node* lnk, std::tm ti);
	~Node();

	void setEntry(std::string str);
	std::string getEntry() const;

	void setLink(Node* lnk);
	Node* getLink() const;
	
	void setTim(std::tm ti);
	std::tm getTim() const;


private:
	Node* link;
	std::string entry;
	std::tm tim;
};
