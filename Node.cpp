#include "Node.h"
#include <iostream>
#include <string>
#include <chrono>

Node::Node()
{
	entry = "";
	link = NULL;

	// Get current time as an "std::chrono::time_point" object named "now"
	auto now = std::chrono::system_clock::now();

	// Converts the "now" object to a time_t object (Unix timestamp) named "now_t"
	std::time_t now_t = std::chrono::system_clock::to_time_t(now);

	// Converts the "now_t" object to a local time stored 
	// in a tm structure named "local"
	std::tm local;
	localtime_s(&local, &now_t);

	tim = local;
}

Node::Node(std::string str, Node* lnk = NULL)
{
	entry = str;
	link = lnk;

	// Get current time as an "std::chrono::time_point" object named "now"
	auto now = std::chrono::system_clock::now();

	// Converts the "now" object to a time_t object (Unix timestamp) named "now_t"
	std::time_t now_t = std::chrono::system_clock::to_time_t(now);

	// Converts the "now_t" object to a local time stored 
	// in a tm structure named "local"
	std::tm local;
	localtime_s(&local, &now_t);

	tim = local;
}

Node::Node(std::string str, Node* lnk, std::tm ti)
{
	entry = str;
	link = lnk;
	tim = ti;
}

Node::~Node()
{
}

void Node::setEntry(std::string str)
{
	entry = str;
}

std::string Node::getEntry() const
{
	return entry;
}

void Node::setLink(Node* lnk)
{
	link = lnk;
}

Node* Node::getLink() const
{
	return link;
}

void Node::setTim(std::tm ti)
{
	tim = ti;
}


std::tm Node::getTim() const
{
	return tim;
}
