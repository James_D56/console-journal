#pragma once

#pragma once
#include "Node.h"
#include <iostream>
#include <string>
#include <chrono>

class JList
{
public:
	JList();
	~JList();

	void append(std::string entry, std::tm ti);
	void clear();

	void goToFront();
	void goToBack();
	void next();

	int getCount() const;
	std::string getE() const;
	std::tm getTime() const;
	Node* getNextPointer() const;

	const JList& operator= (JList&);

private:
	Node* head;
	Node* current;
	Node* tail;

	int count;

	void clearHelp(Node* curr);
};

